/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo;

/**
 *
 * @author Jordy
 */
public class Generador<E> {
     private E ticket;
     private Generador siguiente;
     public Generador(E ticket){
         this.ticket=ticket;
     }
     public Generador(){
         
     }
     public void enlazarSiguiente(Generador n){
        siguiente = n;
    }

    public E getTicket() {
        return ticket;
    }

    public Generador getSiguiente() {
        return siguiente;
    }
    
    
}
