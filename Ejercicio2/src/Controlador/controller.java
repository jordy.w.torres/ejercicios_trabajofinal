/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controlador;

import Modelo.Generador;

/**
 *
 * @author Jordy
 */
public class controller<E>{
    private Generador cabecera;
    private Integer size;
    
    public int Size(){
        return size; 
    }
    public controller(){
        cabecera=null;
        size=0;
    }
    public boolean estaVacia(){
       return(cabecera==null)?true:false;
   }
    
    public void addCabecera(E ticket){
       if (cabecera==null) {
           cabecera=new Generador(ticket);
           
       }else{
           Generador temp = cabecera;
           Generador nuevo = new Generador(ticket);
           nuevo.enlazarSiguiente(temp);
           cabecera= nuevo;
       }
       size++;
   }
    public Object obtennerTicket(int Ind){
        int contador=0;
        
        Generador aux = cabecera;
        while(contador < Ind){
            aux= aux.getSiguiente();
            contador++;
        }
        return aux.getTicket();
    }
    public void eliminar (int ind){
        if(ind==0){
            cabecera = cabecera.getSiguiente();
        }else{ 
            int cont =0;
        Generador aux = cabecera;
        while(cont < ind-1){
            aux = aux.getSiguiente();
            cont++;
        }
        
   aux.enlazarSiguiente(aux.getSiguiente().getSiguiente());
                }
        size--;
    }
    public void eliminarPrimero(){
        cabecera = cabecera.getSiguiente();
        size--;
    }
    
    
}
