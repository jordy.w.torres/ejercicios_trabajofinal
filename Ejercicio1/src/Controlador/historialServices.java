/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controlador;
import Exceptions.PosicionException;
import Modelo.*;
import java.util.Date;
import java.util.LinkedList;
/**
 *
 * @author Jordy
 */
public class historialServices<E> {
   private historial cabecera;
   private int size;
   public historialServices(){
       cabecera=null;
       size=0;
   }
   public boolean estaVacia(){
       return(cabecera==null)?true:false;
   }
   public void addCabecera(E comando,E fecha){
       if (cabecera==null) {
           cabecera=new historial(comando,fecha);
           
       }else{
           historial temp = cabecera;
           historial nuevo = new historial(comando,fecha);
           nuevo.enlazarSiguiente(temp);
           cabecera= nuevo;
       }
       size++;
   }
    public int Size(){
        return size; 
    }
    public Object obtennerComando(int Ind){
        int contador=0;
        
        historial aux = cabecera;
        while(contador < Ind){
            aux= aux.obtenerSiguiente();
            contador++;
        }
        return aux.getComando();
        
    }
    public Object obtenerFecha(int in){
        int contador=0;
        
        historial aux = cabecera;
        while(contador < in){
            aux= aux.obtenerSiguiente();
            contador++;
        }
        return aux.getFecha();
    }
    public void eliminar (int ind){
        if(ind==0){
            cabecera = cabecera.obtenerSiguiente();
        }else{ 
            int cont =0;
        historial aux = cabecera;
        while(cont < ind-1){
            aux = aux.obtenerSiguiente();
            cont++;
        }
        
   aux.enlazarSiguiente(aux.obtenerSiguiente().obtenerSiguiente());
                }
        size--;
    }
    public void eliminarPrimero(){
        cabecera = cabecera.obtenerSiguiente();
        size--;
    }
    
}
    
    
      


