/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo;

/**
 *
 * @author Jordy
 */
public class Pelicula {
    private String Title;
    private int Year ;
    private String Rated;
    private String released;
    private String runtime;
    private String Genre;
    private String Director;
    private String Writer;
    private String Actors;
    private String plot;
    private String Language;
    private String Conuntry;
    private String Award;
    private String Poster;
    private int metascore;
    private int imdbRating;
    private double imdbvotes;
    private String imdbID;
    private String Type;
    private String DVD;
    private String boxOffice;
    private String production;
    private String webside;
    private String reponse;
    
    public Pelicula(String Title, int Year, String Rated, String released,String runtime, String Genre, String Director, String Writer, String Actors, String plot, String Language, String Conuntry, String Award, String Poster,int metascore,int imdbRating,double imdbvotes,String imdbID,String type,String DVD,String boxOffice,String production,String webside,String response) {
        this.Title = Title;
        this.Year = Year;
        this.Rated = Rated;
        this.released = released;
        this.runtime=runtime;
        this.Genre = Genre;
        this.Director = Director;
        this.Writer = Writer;
        this.Actors = Actors;
        this.plot = plot;
        this.Language = Language;
        this.Conuntry = Conuntry;
        this.Award = Award;
        this.Poster = Poster;
        this.metascore = metascore;
        this.imdbvotes=imdbvotes;
        this.imdbID=imdbID;
        this.Type=Type;
        this.DVD=DVD;
        this.boxOffice=boxOffice;
        this.production=production;
        this.webside=webside;
        this.reponse=response;
        
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public int getMetascore() {
        return metascore;
    }

    public void setMetascore(int metascore) {
        this.metascore = metascore;
    }

    public int getImdbRating() {
        return imdbRating;
    }

    public void setImdbRating(int imdbRating) {
        this.imdbRating = imdbRating;
    }

    public double getImdbvotes() {
        return imdbvotes;
    }

    public void setImdbvotes(double imdbvotes) {
        this.imdbvotes = imdbvotes;
    }

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public String getDVD() {
        return DVD;
    }

    public void setDVD(String DVD) {
        this.DVD = DVD;
    }

    public String getBoxOffice() {
        return boxOffice;
    }

    public void setBoxOffice(String boxOffice) {
        this.boxOffice = boxOffice;
    }

    public String getProduction() {
        return production;
    }

    public void setProduction(String production) {
        this.production = production;
    }

    public String getWebside() {
        return webside;
    }

    public void setWebside(String webside) {
        this.webside = webside;
    }

    public String getReponse() {
        return reponse;
    }

    public void setReponse(String reponse) {
        this.reponse = reponse;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public int getYear() {
        return Year;
    }

    public void setYear(int Year) {
        this.Year = Year;
    }

    public String getRated() {
        return Rated;
    }

    public void setRated(String Rated) {
        this.Rated = Rated;
    }

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public String getGenre() {
        return Genre;
    }

    public void setGenre(String Genre) {
        this.Genre = Genre;
    }

    public String getDirector() {
        return Director;
    }

    public void setDirector(String Director) {
        this.Director = Director;
    }

    public String getWriter() {
        return Writer;
    }

    public void setWriter(String Writer) {
        this.Writer = Writer;
    }

    public String getActors() {
        return Actors;
    }

    public void setActors(String Actors) {
        this.Actors = Actors;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String Language) {
        this.Language = Language;
    }

    public String getConuntry() {
        return Conuntry;
    }

    public void setConuntry(String Conuntry) {
        this.Conuntry = Conuntry;
    }

    public String getAward() {
        return Award;
    }

    public void setAward(String Award) {
        this.Award = Award;
    }

    public String getPoster() {
        return Poster;
    }

    public void setPoster(String Poster) {
        this.Poster = Poster;
    }
    
}
