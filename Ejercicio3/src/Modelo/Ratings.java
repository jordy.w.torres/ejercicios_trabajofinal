/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo;

/**
 *
 * @author Jordy
 */
public class Ratings {
    private String source0;
    private String value0;
    private String source1;
    private String value1;
    private String Source2;
    private String value2;

    public Ratings(String source0, String value0, String source1, String value1, String Source2, String value2) {
        this.source0 = source0;
        this.value0 = value0;
        this.source1 = source1;
        this.value1 = value1;
        this.Source2 = Source2;
        this.value2 = value2;
    }

    public String getSource0() {
        return source0;
    }

    public void setSource0(String Source0) {
        this.source0 = Source0;
    }

    public String getValue0() {
        return value0;
    }

    public void setValue0(String value0) {
        this.value0 = value0;
    }

    public String getSource1() {
        return source1;
    }

    public void setSource1(String Source1) {
        this.source1 = Source1;
    }

    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    public String getSource2() {
        return Source2;
    }

    public void setSource2(String Source2) {
        this.Source2 = Source2;
    }

    public String getValue2() {
        return value2;
    }

    public void setValue2(String value2) {
        this.value2 = value2;
    }
    
}
